Ext.define('buscafono.model.Telefono', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'id', type: 'auto'},
            {name: 'nombre', type: 'auto'},
            {name: 'numero', type:'auto'},
            {name: 'anexo', type:'auto'},
			{name: 'imagen', type:'auto'}
        ],
        validations: [
            {type: 'presence',  field: 'nombre', message : "Requerido"},
            {type: 'presence',  field: 'numero', message : "Requerido"},
            {type: 'length',    field: 'numero', min: 2, message : "Minimo 2 Caracteres"},
            {type: 'length',    field: 'nombre', min: 4, message : "Minimo 4 Caracteres"}
        ]
    }
});

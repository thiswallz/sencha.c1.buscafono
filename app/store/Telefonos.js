Ext.define('buscafono.store.Telefonos', {
    extend: 'Ext.data.Store',
    requires: [
    	'buscafono.model.Telefono'
    ],
	config:{
		storeId : 'mgrTelefonoId',
	    model: 'buscafono.model.Telefono',
        autoSync : true,
        clearOnPageLoad:true,
	    proxy:{
            type:'ajax',
            url: 'http://localhost/sencha/c1.core/services/buscafono.php',
            extraParams: {
                type: 'json'
            },
 			actionMethods: {
                create : 'POST',
                read   : 'POST',
                update : 'POST',
                destroy: 'POST'
            },
            reader:{
                type:'json',
                rootProperty:'data'
            },
            listeners:{
                exception:function(proxy, response, orientation){
                    var o = Ext.decode(response.responseText);                    
                    Ext.Msg.alert('Error', o.message);
                }
            }

        }
	}
});
Ext.define('buscafono.view.AgregarFavorito', {
    extend: 'Ext.form.Panel',
    alias: "widget.agregarfavorito",    
    requires: [
        'Ext.TitleBar',
        'Ext.Label', 
        'Ext.Img', 
        'Ext.util.DelayedTask',
        'buscafono.model.Telefono'
    ],
    initialize: function() {
        this.callParent();
        this.telefono = Ext.create('buscafono.model.Telefono', this.recordForm);
        this.setRecord(this.telefono);
    },
    onLimpiarDatos: function(){
        this.setValues({
            nombre: '',
            numero: ''
        });
    },
    onGrabarDatos: function(){
        var me = this;
        var model = Ext.create('buscafono.model.Telefono', this.getValues());
        errors = model.validate()
        if(errors.all.length>0){
            Ext.Msg.alert('Error', "Campo "+ errors.all[0]._field + " " + errors.all[0]._message);
            return;
        }
        var task = Ext.create('Ext.util.DelayedTask', function () {
            me.fireEvent('grabarFavorito', me, model);
        });  
        task.delay(500);      
    },
    recordForm : null,
    config: {
        listeners: [{
            delegate: '#limpiarDatosId',
            event: 'tap',
            fn: 'onLimpiarDatos'
        },{
            delegate: '#grabarDatosId',
            event: 'tap',
            fn: 'onGrabarDatos'
        }],
        fullscreen : true,        
        type: 'vbox',
        items: [{
            xtype : 'toolbar',
            docked: 'top',
            items: [
                {
                    xtype: 'button',
                    ui: 'back',
                    action: 'btnVolverListaNumeros',
                    text:'Volver',
                    itemId: 'volverListaNumerosId'
                }
            ]
        },{
            flex : 1,
            height : '100%',
            items: [{
                label : 'Nombre',
                xtype: 'textfield',
                placeHolder: 'Nombre Contacto',
                itemId: 'nombreContactoId',
                required : true,
                name: 'nombre'                
            },{
                label : 'Numero',
                xtype: 'textfield',
                placeHolder: 'Numero Telefono',
                itemId: 'numeroContactoId',
                required : true,
                name: 'numero'                
            },{
                label : 'Anexo',
                xtype: 'textfield',
                itemId: 'anexoContactoId',
                readOnly : true,
                name: 'anexo'                
            }]
            },            
            {           
                xtype:'container',
                docked : 'bottom',
                flex : 2,
                items:[
                {
                    xtype: 'button',
                    itemId : 'grabarDatosId',
                    ui : 'confirm',
                    text: 'Grabar',
                    margin:7
                },
                {
                    xtype: 'button',
                    ui : 'action',
                    itemId : 'limpiarDatosId',
                    iconMask: true,
                    text: 'Limpiar',
                    margin:7
                    }
                ]
            }
        ]
    }
});

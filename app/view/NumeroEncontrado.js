//definimos el package
Ext.define('buscafono.view.NumeroEncontrado', {
    //componente de donde extiende
    //con esto asume atributos y comportamientos de la clase padre
    extend: 'Ext.Container',
    //alias para llamar despues al panel de la forma xtype : 'loginview'
    alias: "widget.numeroencontrado",    
    //clases que ocuparemos dentro de login
    requires: [
        'Ext.TitleBar', 
        'Ext.dataview.List',
        'Ext.data.proxy.Ajax',
        'Ext.data.Store'
    ],
    initialize: function() {
        this.callParent();
        var me = this;
        me.down('#listaNumerosId').refresh();
    },
    onLlamarTap: function () {
        var me = this;
        var record = me.down('#listaNumerosId').getSelection()[0];
        if(!record){
            Ext.Msg.alert('Lista', "Selecciona un registro");
            return;
        }
        var task = Ext.create('Ext.util.DelayedTask', function () {
            me.fireEvent('llamarNumero', me, record);
        });
        //pequeño delay para simular un loading
        task.delay(500);

    },
    onFavoritoTap: function () {
        var me = this;
        var record = me.down('#listaNumerosId').getSelection()[0];
        if(!record){
            Ext.Msg.alert('Lista', "Selecciona un registro");
            return;
        }
        var task = Ext.create('Ext.util.DelayedTask', function () {
            me.fireEvent('nuevoFavorito', me, record);
        });
        //pequeño delay para simular un loading
        task.delay(500);

    },
    //configs son los items principales de la vista
    config: {
        listeners: [{
            //itemid del boton escuchado
            delegate: '#llamarId',
            //evento
            event: 'tap',
            //funcion derivada
            fn: 'onLlamarTap'
        },{
            delegate: '#numeroFavoritoId',
            event: 'tap',
            fn: 'onFavoritoTap'            

        }],
        fullscreen : true,        
        layout: {
            type: 'vbox',
            pack: 'center'
        },
        items: [
        {
            xtype : 'toolbar',
            docked: 'top',
            items: [
                {
                    xtype: 'button',
                    ui: 'back',
                    action: 'btnVolverBuscarNumero',
                    text:'Volver',
                    itemId: 'volverBuscarNumeroId'
                },
                {
                    xtype: 'button',
                    ui: 'confirm',
                    iconCls: 'star',
                    iconMask: true,
                    action: 'btnNumeroFavorito',
                    text:'Favorito',
                    itemId: 'numeroFavoritoId'                
                },
                { xtype: "spacer" },
                {
                    xtype: 'button',
                    ui: 'action',
                    iconCls: 'action',
                    iconMask: true,
                    text:'Llamar',
                    itemId: 'llamarId'                
                }
            ]

        },
        {
            xtype: 'list',
            itemId: 'listaNumerosId',
            onItemDisclosure : function(record, btn, index) {
                console.log(record)
            },
            //no pintar titulo si esta agrupado
            pinHeaders: false,
            centered: true,
            width: '100%',
            height: '100%',
            itemTpl: '{nombre}, {numero}',
            title: 'Recent Posts',
            store: 'mgrTelefonoId'
        }
        ]
    }
});

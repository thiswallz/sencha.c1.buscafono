//definimos el package
Ext.define('buscafono.view.ListaFavoritos', {
    //componente de donde extiende
    //con esto asume atributos y comportamientos de la clase padre
    extend: 'Ext.Container',
    //alias para llamar despues al panel de la forma xtype : 'loginview'
    alias: "widget.listafavoritos",    
    //clases que ocuparemos dentro de login
    requires: [
        'Ext.TitleBar', 
        'Ext.dataview.List',
        'Ext.data.proxy.Ajax',
        'Ext.data.Store'
    ],
    onLlamarTap: function () {
        var me = this;
        var record = me.down('#listaNumerosFavoritosId').getSelection()[0];
        if(!record){
            Ext.Msg.alert('Lista', "Selecciona un registro");
            return;
        }
        var task = Ext.create('Ext.util.DelayedTask', function () {
            me.fireEvent('llamarNumero', me, record);
        });
        //pequeño delay para simular un loading
        task.delay(500);  
    },
    //configs son los items principales de la vista
    config: {
        listeners: [{
            delegate: '#llamarFavId',
            event: 'tap',
            fn: 'onLlamarTap'
        }],
        fullscreen : true,        
        scrollable: true,
        centered: true,
        width: '100%',
        height: '100%',
        items: [{
            xtype : 'toolbar',
            docked: 'top',
            listeners: {
                 painted:{
                    fn : function() {
                        var me = this;
                        var db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
                        db.transaction(function(tx){
                            tx.executeSql('SELECT * FROM NFAVORITOS', [], function(tx, rs) {
                                var obj = new Array();
                                var len = rs.rows.length;
                                var store = me.up('listafavoritos').down('#listaNumerosFavoritosId').getStore();
                                var records = store.getRange();
                                store.remove(records);
                                for (var i=0; i<len; i++){
                                    obj[i] = {
                                        nombre: rs.rows.item(i).nombre,
                                        numero: rs.rows.item(i).numero,
                                        anexo: rs.rows.item(i).anexo
                                    }
                                }
                                console.log(obj)
                                if(obj.length>0){
                                    me.up('listafavoritos').down('#listaNumerosFavoritosId').setData(obj)             
                                }
                                me.up('listafavoritos').down('#listaNumerosFavoritosId').refresh()
                            }, errorCB);
                        });
                    }
                 }
            },
            items: [
                { xtype: "spacer" },
                {
                    xtype: 'button',
                    ui: 'action',
                    iconCls: 'action',
                    iconMask: true,
                    text:'Llamar',
                    itemId: 'llamarFavId'                
                }
            ]
        },
        {
            xtype: 'list',
            itemId: 'listaNumerosFavoritosId',
            //no pintar titulo si esta agrupado
            pinHeaders: false,
            centered: true,
            width: '100%',
            height: '100%',
            itemTpl: '<img src="http://api.ning.com/files/Hq7T*VHqa0JZwIUOCMbcx1qWCJLPR5f2Xd-YRvXZ4aPJ9y6b0rv7xKGeRoA2XtMHOhxqhRexgbqvhTj*Y9SDyYQiKK5eoQ7s/HTML5_Logo_128.png?crop=1%3A1&width=64" width="32" heigh="32"></img>{nombre}, {numero}, <b>{numero}</b>',
            title: 'Recent Posts',
            store: {
              fields: ['nombre', 'numero', 'anexo'],
              data: [
               {nombre: 'Maxine', numero: 200000, anexo : '1'},
               {nombre: 'Minnie', numero: 30000, anexo : '2'}
              ]
             }        
        }]
    }
});

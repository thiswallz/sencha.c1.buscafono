Ext.define('buscafono.view.Main', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.mainmenuview',
    requires: [
        'Ext.TitleBar'    
    ],
    config: {
        tabBarPosition: 'bottom',
        layout:{
            type:'card',
            animation:'slide'
        },
        items: [
            {
                title: 'Welcome',
                iconCls: 'home',
                styleHtmlContent: true,
                scrollable: true,
                items: {
                    docked: 'top',
                    xtype: 'titlebar'                
                },
                html: [
                    "<h2>Bienvenido a BuscaFono!</h2> <br />",
                    "<h4>Encuentra el numero que estas buscando, guardalo o llama de inmediato!</h4><br />",
                    "<a href='#' >www.buscafono.chileforma.cl</a>"
                ].join("")
            },
            {
                title: 'Buscar',
                iconCls: 'search',
                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar'
                    },
                    {
                        xtype: 'buscarnumero'
                    }
                ]
            },
            {
                title:'Blog',
                iconCls:'star',
                items:[
                {
                    xtype: 'listafavoritos'
                }]
            }
        ]
    }
});

//definimos el package
Ext.define('buscafono.view.BuscarNumero', {
    //componente de donde extiende
    //con esto asume atributos y comportamientos de la clase padre
    extend: 'Ext.form.Panel',
    //alias para llamar despues al panel de la forma xtype : 'loginview'
    alias: "widget.buscarnumero",    
    //clases que ocuparemos dentro de login
    requires: [
        'Ext.TitleBar',
        'Ext.form.FieldSet',
        'Ext.form.Number', 
        'Ext.form.Password', 
        'Ext.Label', 
        'Ext.Img', 
        'Ext.util.DelayedTask'
    ],
    onBuscarAnexo: function(){
        var me = this;
        var anexo = me.down('#anexoNumId').getValue();
        var task = Ext.create('Ext.util.DelayedTask', function () {
            me.fireEvent('buscarPorAnexo', me, anexo);
        });
        //pequeño delay para simular un loading
        task.delay(500);
    },
    //configs son los items principales de la vista
    config: {
        listeners: [{
            delegate: '#btnBuscarNumeroId',
            event: 'tap',
            fn: 'onBuscarAnexo'            
        }],
        scrollable: true,
        centered: true,
        width: '100%',
        height: '100%',
        items: [{
            xtype : 'container',
            layout: {
               type: 'vbox',
               pack: 'center'
            },
            items: [{
                xtype: 'fieldset',
                title: 'Busca el Numero de Telefono',
                items: [{
                    label : 'Anexo',
                    xtype: 'textfield',
                    placeHolder: 'Anexo',
                    itemId: 'anexoNumId',
                    name: 'anexoNum'                
                }]
            },            
            {           
                xtype:'container',
                flex : 1,
                layout : {
                    type : 'hbox',
                    align: 'center'
                },
                items:[
                {
                    xtype: 'button',
                    itemId: 'btnBuscarNumeroId',
                    ui : 'confirm',
                    text: 'Buscar!',
                    width:'48%',
                    margin:7
                },
                {
                    xtype: 'button',
                    ui : 'action',
                    text: 'Limpiar',
                    width:'48%',
                    margin: 7,
                    handler: function() {
                        this.up('buscarnumero').down('#anexoNumId').setValue('')
                    }
                }]
            }
            ]
        }]
    }
});

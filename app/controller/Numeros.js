Ext.define('buscafono.controller.Numeros', {
    extend: 'Ext.app.Controller',
    //load stores
    config: {
        stores: ['Telefonos'],
        refs: {
            buscarNumero: 'buscarnumero',            
            numeroEncontrado: 'numeroencontrado',
            agregarFavorito: 'agregarfavorito',
            mainMenuView: 'mainmenuview',
            listaFavoritos: 'listafavoritos'
        },
        control: {
            'button[action=btnVolverBuscarNumero]': {
                tap: 'volverBusqueda'
            },
            'button[action=btnVolverListaNumeros]': {
                tap: 'volverListaNumeros'
            },
            'button[action=btnLlamarNumero]': {
                tap: 'llamarNumero'
            },
            buscarNumero: {
                buscarPorAnexo: 'buscarPorAnexo'
            },
            numeroEncontrado: {
                llamarNumero: 'llamarNumero',
                nuevoFavorito: 'nuevoFavorito'
            },
            agregarFavorito: {
                grabarFavorito : 'grabarFavorito'
            }
        }
    },
    launch: function() {
        //console.log(this);
    },
    validarFavorito: function(anexo, record){
        var db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
        db.transaction(function(tx){
            tx.executeSql('SELECT count(*) as total FROM NFAVORITOS WHERE anexo = "'+anexo+'"', [], function(tx, rs) {
                if(rs.rows.item(0).total>0){
                    Ext.Msg.alert('Alerta', "El Registro ya se encuentra grabado!");
                    return; 
                }else{
                    db.transaction(function(tx){
                        tx.executeSql('INSERT INTO NFAVORITOS (nombre, numero, anexo) VALUES ("'+record.get('nombre')+'", "'+record.get('numero')+'", "'+record.get('anexo')+'")', [], function(tx, rs) {
                            Ext.Msg.alert('Info', "Anexo Grabado Correctamente");
                        }, errorCB);
                    });                    
                }
            }, errorCB);
        });
    },
    grabarFavorito: function(view, record){
        var db = window.openDatabase("Database", "1.0", "Cordova Demo", 200000);
        this.validarFavorito(record.get('anexo'), record)
    },
    nuevoFavorito: function(view, record){
        var recordForm = {
            nombre: record.get('nombre'),
            numero: record.get('numero'),
            anexo: record.get('anexo')
        }
        Ext.Viewport.animateActiveItem({xtype : 'agregarfavorito', recordForm: recordForm}, {type: 'slide', direction: 'left'});
    },
    llamarNumero: function(view, record) {
        window.open('tel:+'+record.get('numero'));
    },
    volverListaNumeros: function(){
        Ext.Viewport.animateActiveItem({xtype : 'numeroencontrado'}, {type: 'slide', direction: 'right'});
    },
    volverBusqueda: function(){
        Ext.Viewport.animateActiveItem(this.getMainMenuView(), {type: 'slide', direction: 'right'});
    },
    buscarPorAnexo: function(view, anexo) {
        var telStore = Ext.StoreMgr.get('mgrTelefonoId');
        telStore.getProxy().setExtraParams({'fn' : 'getByAnexo', 'anexo' : anexo});
        telStore.on({
            load: 'onTelefonosLoad',
            scope: this
        });
        telStore.load();
    },
    onTelefonosLoad: function() {
        var telStore = Ext.StoreMgr.get('mgrTelefonoId');
        if(telStore.getCount()>0){
            Ext.Viewport.animateActiveItem({xtype : 'numeroencontrado'}, {type: 'slide', direction: 'left'});
        }else{
            Ext.Msg.alert('Alerta', 'No Hay Registros');
        }
    }
});